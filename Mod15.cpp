﻿#include <iostream>
// эта программа содержит 2 варианта выполнения поставленной задачи!
// Нужный вариант раскомментировать!

const int N = 123;

void PrintOddNumbers()      // Функция вывода нечетных чисел
{

    for (int x = 0; x <= N; x++)
    {
        if (x % 2 == 1)
            std::cout << x << "\n";
    }

}

void PrintEvenNumbers()     // Функция вывода четных чисел
{

    for (int x = 0; x <= N; x++)
    {
        if (x % 2 == 0)
            std::cout << x << "\n";
    }

}

int main()        // Вывод четных чисел
{

    setlocale(LC_ALL, "Russian");     //Вывод русского языка в консоль
   
    for (int x = 0; x <= N; x += 2)
    {
        std::cout << x << "\n";
    }


    return 0;

}


//int main()      // Вывод четных или нечетных по выбору пользователя через функции
//{
//
//    setlocale(LC_ALL, "Russian");       //Вывод русского языка в консоль
//
//    int y;
//    std::cout << "Введите 0 для вывода нечетных или 1 для вывода четных: ";
//    std::cin >> y;
//
//    if (y==1)
//    {
//        PrintEvenNumbers();
//    }
//
//    else
//    {
//        PrintOddNumbers();
//    }
//
//    return 0;
//}



